
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SYM
{
    unsigned char  ch;
    float        freq;
    int           cnt;
    int       codelen;
    char    code[256];
    struct SYM  *left;
    struct SYM *right;
};

struct SYM* buildTree(struct SYM *psym[], int N)
{
    struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
    temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];
    temp->code[0] = 0;
    if (N == 2)
        return temp;
    int pos = N - 2;
    while (pos>0 && psym[pos]->freq<temp->freq)
    {
        psym[pos] = psym[pos - 1];
        pos--;
    }
    psym[pos] = temp;
    return buildTree(psym, N - 1);
}

void makeCodes(struct SYM *root)
{
    root->codelen = strlen(root->code);
    if (root->left)
    {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
}

void recursiveFree(struct SYM *root)
{
    if (root->left)
    {
        recursiveFree(root->left);
    }
    if (root->right)
    {
        recursiveFree(root->right);
    }
    free(root);
}

union bin4
{
    float f;
    int i;
    unsigned char s[4];
};

void fprinti(int val, FILE* fp)
{
    union bin4 bin;
    bin.i = val;
    int i;
    for (i = 0; i<4; i++)
        fputc(bin.s[i], fp);
}

void fprintfl(float val, FILE* fp)
{
    union bin4 bin;
    bin.f = val;
    int i;
    for (i = 0; i<4; i++)
        fputc(bin.s[i], fp);
}

union bin4 freadb(FILE* fp)
{
    union bin4 bin;
    int i;
    for (i = 0; i<4; i++)
        bin.s[i] = fgetc(fp);
    return bin;
}

void freads(char *s, int len, FILE* fp)
{
    int i;
    for (i = 0; i<len; i++)
        s[i] = fgetc(fp);
    s[len] = '\0';
}

union CODE
{
    unsigned char ch;
    struct {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    } byte;
};

unsigned char pack(char buf[])
{
    union CODE code;
    code.byte.b1 = buf[0] - '0';
    code.byte.b2 = buf[1] - '0';
    code.byte.b3 = buf[2] - '0';
    code.byte.b4 = buf[3] - '0';
    code.byte.b5 = buf[4] - '0';
    code.byte.b6 = buf[5] - '0';
    code.byte.b7 = buf[6] - '0';
    code.byte.b8 = buf[7] - '0';
    return code.ch;
}

void unpack(unsigned char ch, char buf[])
{
    union CODE code;
    code.ch = ch;
    buf[0] = code.byte.b1 + '0';
    buf[1] = code.byte.b2 + '0';
    buf[2] = code.byte.b3 + '0';
    buf[3] = code.byte.b4 + '0';
    buf[4] = code.byte.b5 + '0';
    buf[5] = code.byte.b6 + '0';
    buf[6] = code.byte.b7 + '0';
    buf[7] = code.byte.b8 + '0';
}

int main(int argc, char **argv)
{
    if (argc<4 || (strcmp(argv[1], "-c") != 0 && strcmp(argv[1], "-d") != 0))
    {
       printf("Incorrect option line, should be: [-c|-d] [InputFile] [OutputFile]");
        return 0;
    }

    if (strcmp(argv[1], "-c") == 0)
    {
        char *input_filename = argv[2];
        char *output_filename = argv[3];
        char ch;
        FILE *fp_in = fopen(input_filename, "rb");
        if (!fp_in)
        {
            printf("Error: file %s not found\n", input_filename);
            return 0;
        }
        struct SYM** allsyms = (struct SYM**)malloc(sizeof(struct SYM*) * 256);
        int i;
        for (i = 0; i<256; i++)
            allsyms[i] = 0;
        int filesize = 0;
        int count = 0;
        while (1)
        {
            unsigned char uch = fgetc(fp_in);
            if (feof(fp_in)) break;
            if (!allsyms[uch]) {
                allsyms[uch] = (struct SYM*)malloc(sizeof(struct SYM));
                allsyms[uch]->ch = uch;
                allsyms[uch]->cnt = 0;
                allsyms[uch]->left = allsyms[uch]->right = 0;
                count++;
            }
            allsyms[uch]->cnt++;
            filesize++;
        }
        struct SYM** psym = (struct SYM**)malloc(sizeof(struct SYM*)*count);
        int j;
        for (i = 0, j = 0; i<256; i++)
            if (allsyms[i])
            {
                allsyms[i]->freq = (float)allsyms[i]->cnt / filesize;
                psym[j++] = allsyms[i];
            }
        for (i = 0; i<count; i++)
            for (j = 0; j<count - i - 1; j++)
                if (psym[j]->freq<psym[j + 1]->freq) {
                    struct SYM* tmp = psym[j];
                    psym[j] = psym[j + 1];
                    psym[j + 1] = tmp;
                }
        struct SYM* root = psym[0];
        if (count == 1)
            strcat(psym[0]->code, "0");
        else
            root = buildTree(psym, count);
        makeCodes(root);
        int compsize = 0;
        for (i = 0; i<256; i++)
            if (allsyms[i])
                compsize += allsyms[i]->codelen*allsyms[i]->cnt;
        int tail = 0;
        while ((compsize + tail) % 8 != 0) tail++;
        char *compbuf = (char*)malloc(sizeof(char)*(compsize + tail));
        int pos = 0;
        fclose(fp_in);
        fp_in = fopen(input_filename, "rb");
        for (i = 0; i<filesize; i++)
        {
            unsigned char uch = fgetc(fp_in);
            for (j = 0; j<allsyms[uch]->codelen; j++)
                compbuf[pos++] = allsyms[uch]->code[j];
        }
        for (i = 0; i<tail; i++)
            compbuf[pos++] = '0';

        FILE *fp_out = fopen(output_filename, "wb");
        fputs("HFM", fp_out);
        fprinti(count, fp_out);
        for (i = 0; i<256; i++)
            if (allsyms[i])
            {
                fputc(allsyms[i]->ch, fp_out);
                fprintfl(allsyms[i]->freq, fp_out);
            }
        fprinti(compsize, fp_out);
        fprinti(tail, fp_out);
        fprinti(filesize, fp_out);
        for (i = 0; i<compsize + tail; i += 8)
            fputc(pack(compbuf + i), fp_out);
        fclose(fp_out);
        free(allsyms);
        recursiveFree(root);
        free(compbuf);
    }
    else
    {
        char *input_filename = argv[2];
        char *output_filename = argv[3];
        FILE *fp_in = fopen(input_filename, "rb");
        if (!fp_in)
        {
            printf("Error: file %s not found\n", input_filename);
            return 0;
        }
        char sign[4];
        freads(sign, 3, fp_in);
        if (strcmp(sign, "HFM") != 0)
        {
            printf("Error: file %s is not a compressed archive\n", input_filename);
            fclose(fp_in);
            return 0;
        }
        int count = freadb(fp_in).i;
        if (count<0 || count>256)
        {
            printf("Error: file %s is not a compressed archive\n", input_filename);
            fclose(fp_in);
            return 0;
        }
        struct SYM** allsyms = (struct SYM**)malloc(sizeof(struct SYM*) * 256);
        int i;
        for (i = 0; i<256; i++)
            allsyms[i] = 0;
        for (i = 0; i<count; i++)
        {
            unsigned char uch = fgetc(fp_in);
            float freq = freadb(fp_in).f;
            allsyms[uch] = (struct SYM*)malloc(sizeof(struct SYM));
            allsyms[uch]->ch = uch;
            allsyms[uch]->cnt = 0;
            allsyms[uch]->left = allsyms[uch]->right = 0;
            allsyms[uch]->freq = freq;
        }

        struct SYM** psym = (struct SYM**)malloc(sizeof(struct SYM*)*count);
        int j;
        for (i = 0, j = 0; i<256; i++)
            if (allsyms[i]) {
                psym[j++] = allsyms[i];
            }
        for (i = 0; i<count; i++)
            for (j = 0; j<count - i - 1; j++)
                if (psym[j]->freq<psym[j + 1]->freq) {
                    struct SYM* tmp = psym[j];
                    psym[j] = psym[j + 1];
                    psym[j + 1] = tmp;
                }
        struct SYM* root = psym[0];
        if (count == 1)
            strcat(psym[0]->code, "0");
        else
            root = buildTree(psym, count);
        makeCodes(root);

        int compsize = freadb(fp_in).i;
        int tail = freadb(fp_in).i;
        int filesize = freadb(fp_in).i;

        char *compbuf = (char*)malloc(sizeof(char)*(compsize + tail));

        for (i = 0; i<compsize + tail; i += 8)
        {
            unsigned char uch = fgetc(fp_in);
            unpack(uch, compbuf + i);
        }

        FILE *fp_out = fopen(output_filename, "wb");
        struct SYM* cur = root;
        int rfilesize = 0;
        for (i = 0; i<compsize; i++)
        {
            if (compbuf[i] == '0')
                cur = cur->left;
            else
                cur = cur->right;
            if (!cur->left && !cur->right)
            {
                rfilesize++;
                fputc(cur->ch, fp_out);
                cur = root;
            }
        }
        fclose(fp_out);
        if (filesize != rfilesize)
        {
            printf("Input file size %d not equal to uncopmressed file size %d\n", filesize, rfilesize);
        }
        free(allsyms);
        recursiveFree(root);
        free(compbuf);
    }
    return 0;
}