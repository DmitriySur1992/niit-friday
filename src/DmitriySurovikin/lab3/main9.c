/*
�������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE
*/

#include <stdio.h>

char str[80] = { 0 };
int i = 0;
int mem = 0;
int imem = 0;
int count = 1;

int main()
{
    printf("Enter an arbitrary string: \n");
    fgets(str, 80, stdin);

    for (i = 0; str[i] != '\n'; i++)
    {
        if (str[i] == str[i + 1])
        {
            count++;
        }
        else
        {
            if (mem < count)
            {
                mem = count;
                imem = str[i];
            }

            count = 1;
        }
    }
    printf("%d ", mem);
    for (; mem>0; mem--)
        printf("%c", imem);
    return 0;
}
